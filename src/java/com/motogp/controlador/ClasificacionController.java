/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.motogp.controlador;

import com.motogp.entidades.Piloto;
import com.motogp.modelo.Nodo;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author juand
 */
@Named(value = "clasificacionController")
@SessionScoped
public class ClasificacionController implements Serializable {
private ListaPilotosDE lista = new ListaPilotosDE();
private Piloto pilotoSeleccionado = new Piloto() ;

private Date tiempo ; 
private Nodo nodoMostrar;

    public Nodo getNodoMostrar() {
        return nodoMostrar;
    }

    public void setNodoMostrar(Nodo nodoMostrar) {
        this.nodoMostrar = nodoMostrar;
    }


    /**
     * Creates a new instance of ClasificacionController
     */
    public ClasificacionController() {
    }
    
        public ListaPilotosDE getLista() {
        return lista;
    }

    public void setLista(ListaPilotosDE lista) {
        this.lista = lista;
    }

    public Date getTiempo() {
        return tiempo;
    }

    public void setTiempo(Date tiempo) {
        this.tiempo = tiempo;
    }

  
  

    public Piloto getPilotoSeleccionado() {
        return pilotoSeleccionado;
    }

    public void setPilotoSeleccionado(Piloto pilotoSeleccionado) {
        this.pilotoSeleccionado = pilotoSeleccionado;
    }

   public void adicionarClasificacion()
   {
       this.lista.adicionarNodoFinal(new Nodo(pilotoSeleccionado, tiempo));
       irAlPrimero();
       //limpiarClasificacion();
   }
    public void irAlPrimero()
    {
        if(lista.getCabeza()!=null)
        {
            nodoMostrar=lista.getCabeza();
        }
    }
    public void limpiarClasificacion()
    {
       tiempo= new Date();
       tiempo.setHours(0);
       tiempo.setMinutes(0);
       tiempo.setSeconds(0);
       pilotoSeleccionado = new Piloto();
    }
    
     public void irAlSiguiente()
    {
        
        if( nodoMostrar.getSiguiente()!=null)
            nodoMostrar=nodoMostrar.getSiguiente();
        
    }
      public void irAlAnterior()
    {
        
        if( nodoMostrar.getAnterior()!=null)
            nodoMostrar=nodoMostrar.getAnterior();
        
    }
}
