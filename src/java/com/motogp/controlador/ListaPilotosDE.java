/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.motogp.controlador;

import com.motogp.modelo.Nodo;
import java.io.Serializable;

/**
 *
 * @author juand
 */
public class ListaPilotosDE implements  Serializable{
    private Nodo cabeza;
    
    public String adicionarNodoFinal(Nodo dato) {
        if (cabeza == null) {
            cabeza = dato;
        } else {
            Nodo temp = cabeza;
            while (temp.getSiguiente() != null) {
                temp = temp.getSiguiente();
            }
            Nodo nuevo = dato;
            temp.setSiguiente(nuevo);
            nuevo.setAnterior(temp);
        }
        return "Adicionado con éxito";

    }

    public Nodo getCabeza() {
        return cabeza;
    }

    public void setCabeza(Nodo cabeza) {
        this.cabeza = cabeza;
    }

}
