/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.motogp.controlador;

import com.motogp.entidades.Usuario;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author juand
 */
@Stateless
public class UsuarioFacade extends AbstractFacade<Usuario> {

    @PersistenceContext(unitName = "moto_gpPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsuarioFacade() {
        super(Usuario.class);
    }
    
    
      public Usuario encontrarUsuarioxCorreo(String correo)
    {
        Query q= em.createNamedQuery("Usuario.findByCorreo", Usuario.class)
                .setParameter("correo", correo);
        
       
        List<Usuario> list= q.getResultList();
        
        if(list.isEmpty())
        {
            return null;
        }
        else
        {
            return list.get(0);
        }    
}
}
